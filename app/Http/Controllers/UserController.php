<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function user_login_check(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email',
            'password' =>  'required'
        ]);
        $email = $request->input('email');
        $password = $request->input('password');

        $user = User::where('email', '=', $email)->first();
        if (!$user) {
            return back()->with('error', 'Login Fail, please check email id');
        }
        if (!Hash::check($password, $user->password)) {
            return back()->with('error', 'Login Fail, pls check password');
        }
        if ($user->user_type == 1) {
            session()->put('user_type', 1);
            return redirect('home');
        } else {
            session()->put('user', 0);
            return redirect('home');
        }
    }

    public function user_register(Request $data)
    {
        if (session()->get('user_type') == 1) {
            $this->validate($data, [
                'name' => 'required',
                'email' => 'required|unique:users,email',
                'password' => 'required',
                'profile_image' => 'required'
            ]);
            $file_extention = $data->profile_image->getClientOriginalExtension();
            $file_name = time() . rand(99, 999) . ".".$file_extention;
            $data->profile_image->move(public_path('/user/image'), $file_name);

            User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'profile_image' => $file_name,
                'user_type' => 0
            ]);

            return redirect('home');
        } else {
            return redirect('/');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->forget('user_type');
        return redirect('/');
    }
}

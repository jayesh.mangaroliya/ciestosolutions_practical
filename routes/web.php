<?php

use App\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::post('/user_login_check', 'UserController@user_login_check')->name('user_login_check');


Route::get('/home', function () {
    if (session()->get('user_type') == 1 or session()->get('user_type') == 0) {
        $user_list = User::where('user_type', 0)->get();
        return view('welcome', compact('user_list'));
    } else {
        return redirect('/');
    }
})->name('home');


Route::get('/register', function () {
    if (session()->get('user_type') == 1) {
        return view('register');
    } else {
        return redirect('/');
    }
})->name('register');

Route::post('/user-register', 'UserController@user_register')->name('user-register');

Route::get('/logout', 'UserController@logout')->name('logout');

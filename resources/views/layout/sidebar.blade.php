<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
    <div class="sb-sidenav-menu">
        <div class="nav">
            @if (session()->get('user_type') == 1)

            <a class="nav-link" href="{{ route('home') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                User List
            </a>
            @endif
        </div>
    </div>
    @if (session()->get('user_type') == 1)
    <div class="sb-sidenav-footer">
        <div class="small">Logged in as:</div>
        Admin
    </div>
    @else
    <div class="sb-sidenav-footer">
        <div class="small">Logged in as:</div>
        User
    </div>
    @endif
</nav>

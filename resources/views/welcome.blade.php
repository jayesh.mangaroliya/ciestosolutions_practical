@extends('master')
@section('content')
<div class="container-fluid px-4">

    @if (session()->get('user_type') == 1)
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-user me-1"></i>
           User List

            <a href="{{ route('register') }}" class="btn btn-primary" style="float: right;">Add User</a>
        </div>
        <div class="card-body">
            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Profile Photo</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Profile Photo</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($user_list as $user_lists)
                      <tr>
                        <td>{{ $user_lists->name }}</td>
                        <td>{{ $user_lists->email }}</td>
                        <td><img src="{{ asset('user/image/'.$user_lists->profile_image) }}" height="100px" width="100px"/></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <h4>Welcome User</h4>
    @endif
</div>
@endsection

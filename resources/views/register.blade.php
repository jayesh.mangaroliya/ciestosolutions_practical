@extends('master')
@section('content')
<main>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Create Account</h3></div>
                    <div class="card-body">
                        <form method="post" action="{{ route('user-register') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row mb-3">
                                <div class="col-md-12">
                                    <div class="form-floating mb-3 mb-md-0">
                                        <input class="form-control" id="inputFirstName" type="text" name="name" placeholder="Enter your first name" value="{{ old('name') }}" />
                                        <label for="inputFirstName">First name</label>
                                        <span style="color: red">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control" id="inputEmail" type="email" name="email" placeholder="name@example.com" value="{{ old('email') }}"/>
                                <label for="inputEmail">Email address</label>
                                <span style="color: red">{{ $errors->first('email') }}</span>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div class="form-floating mb-3 mb-md-0">
                                        <input class="form-control" id="inputPassword" type="password" name="password" placeholder="Create a password" />
                                        <label for="inputPassword">Password</label>
                                        <span style="color: red">{{ $errors->first('password') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating mb-3 mb-md-0">
                                        <input class="form-control" id="inputPasswordConfirm" type="file" name="profile_image" placeholder="Image" value="{{ old('profile_image') }}"/>
                                        <label for="inputPasswordConfirm">Image</label>
                                        <span style="color: red">{{ $errors->first('profile_image') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4 mb-0">
                                <div class="d-grid"><button type="submit" class="btn btn-primary btn-block">Create Account</button></div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>
@endsection
